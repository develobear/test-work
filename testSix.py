from prometheus_client import start_http_server, Gauge
import time
from ClientMain import ClientMain
from WebsocketClientMain import WebsocketClientMain

client_main = ClientMain()

usdt_quote_assets_symbols = client_main.get_quote_asset_by_symbol('USDT')

websocket_client = WebsocketClientMain(symbols=usdt_quote_assets_symbols)

websocket_client.websocket_connection()

if __name__ == '__main__':

    symbol_gauges = {}

    for symbol in usdt_quote_assets_symbols:
        p_s_gauge = Gauge(symbol + '_price_spread_gauge', symbol + ' Price Spread')
        a_d_gauge = Gauge(symbol + '_abs_delta_gauge', symbol + ' Absolute Delta')
        symbol_gauges[symbol] = {}
        symbol_gauges[symbol]['price_spread_gauge'] = p_s_gauge
        symbol_gauges[symbol]['abs_delta_gauge'] = a_d_gauge

    start_http_server(8080)

    while True:
        for symbol in usdt_quote_assets_symbols:
            if websocket_client.get_latest_price_spread(symbol):
                price_spread = websocket_client.latest_responses[symbol][-1]['price_spread']
                abs_delta = websocket_client.latest_responses[symbol][-1]['absolute_delta']
                symbol_gauges[symbol]['price_spread_gauge'].set(price_spread)
                symbol_gauges[symbol]['abs_delta_gauge'].set(abs_delta)

        time.sleep(10)