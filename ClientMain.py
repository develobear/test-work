import os
from binance.spot import Spot as Client


class ClientMain:

    def __init__(self):
        self.binance_api = os.environ.get('binance_api')
        self.binance_secret = os.environ.get('binance_secret')
        self.entry_point = Client(key=self.binance_api, secret=self.binance_secret, base_url='https://testnet.binance.vision')

    def get_quote_asset_by_symbol(self, symbol: str) -> list:
        result = []
        exchange_info = self.entry_point.exchange_info()
        if exchange_info and exchange_info['symbols']:
            for item in exchange_info['symbols']:
                if item['quoteAsset'] == symbol:
                    result.append(item['symbol'])
        return result

    def get_tickers(self, needed_attribute: str, symbols: list) -> list:
        result = []
        if symbols and needed_attribute:
            for symbol in symbols:
                ticker = self.entry_point.ticker_24hr(symbol)
                if ticker:
                    for attribute in ticker:
                        if attribute == needed_attribute:
                            result.append((symbol, ticker[attribute], float(ticker[attribute])))
        return result



