from ClientMain import ClientMain

client_main = ClientMain()

usdt_quote_assets_symbols = client_main.get_quote_asset_by_symbol('USDT')

result = client_main.get_tickers('count', usdt_quote_assets_symbols)

if result:
    sorted_result = sorted(result, reverse=True, key=lambda elem: elem[2])
    for i in range(5):
        print("{0} - {1}".format(sorted_result[i][0], sorted_result[i][1]))