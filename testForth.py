from ClientMain import ClientMain

client_main = ClientMain()

usdt_quote_assets_symbols = client_main.get_quote_asset_by_symbol('USDT')

if usdt_quote_assets_symbols:
    for symbol in usdt_quote_assets_symbols:
        ticker = client_main.entry_point.ticker_24hr(symbol)
        price_spread = float(ticker['askPrice']) - float(ticker['bidPrice'])
        print('Price spread for {0} is {1}'.format(symbol, price_spread))