from ClientMain import ClientMain

client_main = ClientMain()

BTC_quote_assets_symbols = client_main.get_quote_asset_by_symbol('BTC')


def get_total_notion_value(prices):
    res = 0
    if prices:
        for i in prices:
            res += float(i[0]) * float(i[1])
    return res


if BTC_quote_assets_symbols:
    for symbol in BTC_quote_assets_symbols:
        depth = client_main.entry_point.depth(symbol, limit=200)
        bids = get_total_notion_value(depth['bids'])
        asks = get_total_notion_value(depth['asks'])
        print('symbol - {0}: bids - {1}; asks - {2}.'.format(symbol, bids, asks))

