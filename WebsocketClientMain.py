import time
import logging
from binance.lib.utils import config_logging
from binance.websocket.spot.websocket_client import SpotWebsocketClient as Client


class WebsocketClientMain:

    def __init__(self, symbols: list):
        self.symbols = symbols
        self.latest_responses = {}
        self.entry_point = Client(stream_url='wss://testnet.binance.vision')

        if self.symbols:
            for symbol in self.symbols:
                self.latest_responses[symbol] = []

    def get_latest_price_spread(self, symbol: str):
        if symbol in list(self.latest_responses):
            if self.latest_responses[symbol]:
                return self.latest_responses[symbol][-1]["price_spread"]
        return None

    def set_latest_price_spread(self, symbol: str, price_spread: dict):
        if symbol in list(self.latest_responses):
            self.latest_responses[symbol].append(price_spread)

    @staticmethod
    def get_absolute_delta(a, b):
        result = 0
        if a < b:
            result = b - a
        elif b < a:
            result = a - b
        return result

    def ticker_handler(self, response: dict):
        response_symbol = ''
        ask_price = 0
        bid_price = 0
        for key in response:
            if key == 'b':
                ask_price = float(response[key])
            elif key == 'a':
                bid_price = float(response[key])
            elif key == 's':
                response_symbol = response[key]
        if response_symbol:
            last_price_spread = self.get_latest_price_spread(response_symbol)
            price_spread = ask_price - bid_price
            absolute_delta = 0

            if last_price_spread is not None:
                absolute_delta = self.get_absolute_delta(price_spread, last_price_spread)

            response_data = {
                "prev_price_spread": last_price_spread,
                "price_spread": price_spread,
                "absolute_delta": absolute_delta
            }

            self.set_latest_price_spread(response_symbol, response_data)

            print('Price spread for {0} is {1}.'.format(response_symbol, price_spread))
            print('Absolute delta: {0}.'.format(absolute_delta))

            time.sleep(10)

    def websocket_connection(self):
        config_logging(logging, logging.DEBUG)
        self.entry_point.start()

        stream_id = 1
        for symbol in self.symbols:
            self.entry_point.ticker(
                symbol=symbol.lower(),
                id=stream_id,
                callback=self.ticker_handler,
            )
            stream_id += 1

