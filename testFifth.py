from ClientMain import ClientMain
from WebsocketClientMain import WebsocketClientMain

client_main = ClientMain()

usdt_quote_assets_symbols = client_main.get_quote_asset_by_symbol('USDT')

websocket_client = WebsocketClientMain(symbols=usdt_quote_assets_symbols)

websocket_client.websocket_connection()

